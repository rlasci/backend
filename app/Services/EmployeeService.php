<?php


namespace App\Services;

use App\Employee;
use App\Http\Resources\EmployeeResource;
use App\Mail\EmployeeMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class EmployeeService
{
    private $employees;

    public function __construct(Employee $employees)
    {
        $this->employees = $employees;
    }

    public function index()
    {
        try{
            $employees = auth()->user()->employees;
            $data = EmployeeResource::collection($employees);
            return ['data' => $data];
        }catch (\Exception $e){
            Log::error('Erro ao listar os colaboradores: '. $e);
            return ['error' => 'Não foi possível carregar os dados.'.$e];
        }
    }

    public function importCsv($request)
    {
        $path = $request->file('employees')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        $csv_data = collect(array_slice($data, 1));
        try {
            $csv_data->map(function ($employee){
                array_push($employee, auth('api')->user()->id,uniqid(),date('Y-m-d'),date('Y-m-d'));
                $insert = array_combine($this->employees->getFillable(),$employee);
                if ($this->employees->where('document', $insert['document'])->where('email', $insert['email'])->exists())
                {
                    $update_employee = $this->employees->where('document', $insert['document'])->where('email', $insert['email'])->get()->first();
                    $update_employee->update([
                        'name' => $insert['name'],
                        'email' => $insert['email'],
                        'city'  => $insert['city'],
                        'state' => $insert['state'],
                        'start_date' => $insert['start_date'],
                        'manager_id' => $insert['manager_id'],
                        'updated_at' => $insert['updated_at']
                    ]);
                }else{
                    $this->employees->create($insert);
                }
            });
            $this->sendSuccessEmail();
            return ['data' => $this->employees->latest()->first()];
        }catch (\Exception $e){
            Log::error('Erro ao importar os colaboradores: '. $e);
            return ['error' => 'Não foi possível importar.'.$e];
        }
    }

    public function destroy($id)
    {
        try {
            $employee = $this->employees->where('document', $id)->firstOrFail();
            $employee->delete();
            return ['data' => 'Colaborador removido.'];
        }catch (\Exception $e){
            Log::error('Erro ao remover colaborador  '.$id.'. Erro: '. $e);
            return ['error' => 'Não foi possível remover o colaborador.'];
        }
    }

    public function sendSuccessEmail()
    {
        try {
            Mail::to(auth()->user())->queue(new EmployeeMail());
        }catch (\Exception $e){
            throw new \Exception('Não foi possível enviar o email.'.$e);
        }
    }
}
