<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'name', 'email', 'document', 'city' , 'state', 'start_date', 'manager_id','id','created_at','updated_at'
    ];
}
