<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');

        if ($token = auth('api')->attempt($input)) {
            return response()->json(['access_token' => $token], 200);
        }
        return response()->json([ 'error' => 'Não autorizado.'], 401);

    }
}
