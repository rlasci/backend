<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Services\EmployeeService;


class EmployeeController extends Controller
{
    public $employee;


    public function __construct(EmployeeService $employeeService)
    {
        $this->employee = $employeeService;
    }

    public function index()
    {
        $data = $this->employee->index();
        return isset($data['data']) ? response()->json($data['data'], 200) : response()->json($data['error'], 500);
    }

    public function importCsv(EmployeeRequest $request)
    {
        $data = $this->employee->importCsv($request);
        return isset($data['data']) ? response()->json($data['data'], 201) : response()->json($data['error'], 500);
    }

    public function destroy(string $document)
    {
        $data = $this->employee->destroy($document);
        return isset($data['data']) ? response()->json($data['data'],204) : response()->json($data['error'], 404);
    }
}
