<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'document' => $this->document,
            'city'  => $this->city,
            'state' => $this->state,
            'start_date' => $this->start_date,
            'manager_id' => $this->manager_id,
         ];
    }
}
