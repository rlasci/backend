<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::namespace('Api')->group(function () {
    /* Auth */
    Route::prefix('auth')->name('auth.')->group(function (){
        Route::post('login', 'LoginController@login')->name('login');
    });

    Route::group(['middleware' => ['jwt']], function () {
        /* Employees */
        Route::prefix('employees')->name('employees.')->group(function () {
            Route::get('', 'EmployeeController@index')->name('index');
            Route::delete('{document}', 'EmployeeController@destroy')->name('destroy');
            /* Import */
            //Route::prefix('import')->name('import.')->group(function (){
            Route::post('csv', 'EmployeeController@importCsv')->name('csv');
            //});
        });
    });
});
